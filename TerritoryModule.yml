workflow:
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH

stages:
  - static_analysis
  - softload
  - rom
  - deploy
  - cleanup

gitattributes:
  stage: static_analysis
  tags: [ cross ]
  script:
    - >
        function top_dir {
        if [ -d "$1" ] && ! grep -q "^$1/\*\* " .gitattributes; then
        echo ".gitattributes lacks pattern for top-level $1 directory"; false;
        fi
        };
        function nested_dir {
        if [ $(find . -mindepth 2 -type d -name "$1" | wc -c) -ne 0 ] &&
        ! grep -q "^\*\*/$1/\*\* " .gitattributes; then
        echo ".gitattributes lacks pattern for nested $1 directory"; false;
        fi
        };
        function dir_type {
        top_dir "$1";
        nested_dir "$1";
        };
        function file_type {
        if [ $(find . -type f -name "*,$1" | wc -c) -ne 0 ] &&
        ! grep -q "^\*,$1 " .gitattributes; then
        echo ".gitattributes lacks pattern for filetype $1"; false;
        fi
        };
        dir_type  Hdr;
        dir_type  hdr;
        dir_type  s;
        dir_type  awk;
        dir_type  bas;
        file_type ffb;
        file_type fb1;
        dir_type  c;
        dir_type  h;
        dir_type  x;
        dir_type  CMHG;
        dir_type  cmhg;
        dir_type  c++;
        file_type fe1;
        dir_type  pl;
        file_type 102;
  allow_failure: true

gitignore:
  stage: static_analysis
  tags: [ cross ]
  script:
    - test -f .gitignore
  allow_failure: true

license:
  stage: static_analysis
  tags: [ cross ]
  script:
    - >
        test -f LICENSE ||
        test -f License.txt ||
        test -f LICENCE ||
        test -f Licence ||
        test -f COPYING ||
        test -f Copying ||
        test -f COPYINGLIB ||
        test -f COPYING.LESSER ||
        test -f Artistic
  allow_failure: true

versionnum:
  stage: static_analysis
  tags: [ cross ]
  script:
    - test -f VersionNum
  allow_failure: true

head_log:
  stage: static_analysis
  tags: [ cross ]
  script:
    - >
        git log -1 --pretty=format:%B | awk '
        NR==1 && length($0)>70 { print "Commit log summary line exceeds 70 characters"; result=1 }
        NR==2 && length($0)>0  { print "Commit log requires a single-line summary followed by a blank line"; result=1 }
        NR>=2 && length($0)>80 { print "Commit log line "NR" exceeds 80 characters"; result=1 }
        END { exit result }'
  allow_failure: true

merge_log:
  stage: static_analysis
  tags: [ cross ]
  rules:
    - if: $CI_MERGE_REQUEST_ID
  script:
    - >
        git remote show | grep target > /dev/null && git remote remove target || true;
        git remote add -t $CI_MERGE_REQUEST_TARGET_BRANCH_NAME -f target $CI_MERGE_REQUEST_PROJECT_URL.git;
        has_versionnum=1; custom_versionnum=0;
        if test -f VersionNum; then grep -q "This file is automatically maintained by srccommit, do not edit manually." VersionNum || custom_versionnum=1; else has_versionnum=0; fi;
        h=$(git rev-parse HEAD);
        for r in $(git rev-list --reverse $(git merge-base HEAD target/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME)..HEAD);
        do
        git log -1 --pretty=format:%B $r | awk '
        BEGIN { keywords=0; nochange=0; notag=0; tag=""; printf("\033[91m") }
        /!NoChange|!NoTag|!Tag\([^)]+\)/ { ++keywords }
        /!NoChange/ { if ("'$r'"=="'$h'") nochange=1; else { print "Commit '$r' is not tip of merge request and uses !NoChange keyword in log"; result=1 } }
        /!NoTag/ { if ("'$r'"=="'$h'") notag=1; else { print "Commit '$r' is not tip of merge request and uses !NoTag keyword in log"; result=1 } }
        /!Tag\([^)]+\)/ { if ("'$r'"=="'$h'") tag=gensub(/^.*!Tag\(([^)]+)\).*$/, "\\1", "g");
        else { print "Commit '$r' is not tip of merge request and uses !Tag keyword in log"; result=1 } }
        NR==1 && length($0)>70 { print "Commit '$r' log summary line exceeds 70 characters"; result=1 }
        NR==2 && length($0)>0  { print "Commit '$r' log requires a single-line summary followed by a blank line"; result=1 }
        NR>=2 && length($0)>80 { print "Commit '$r' log line "NR" exceeds 80 characters"; result=1 }
        END {
        if (keywords > 1) { print "Commit '$r' uses multiple keywords in log"; result = 1 }
        if ("'$r'"=="'$h'") {
        if (!'$has_versionnum' && tag=="") { print "Either create a VersionNum file or use !Tag keyword in log"; result=1 }
        if ('$custom_versionnum' && tag=="" && notag==0) { print "Non-standard VersionNum file requires either !Tag or !NoTag keyword in log"; result=1 }
        if (tag!="") {
        if ('$has_versionnum' && !'$custom_versionnum') { print "!Tag keyword must not be used with standard VersionNum file"; result=1 }
        if (system("git check-ref-format \"tags/" tag "\"")) { print "Invalid tag name"; result = 1 }
        else if (!system("git ls-remote --exit-code --tags target " tag " > /dev/null")) { print "Tag already exists in destination repository"; result=1 }
        }
        printf("\033[39m")
        }
        exit result;
        }';
        done
  allow_failure: true

makefile:
  stage: static_analysis
  tags: [ cross ]
  script:
    - >
        for f in $(find *
        -name "makefile" -o
        -name "*Make*" -o
        -name "*.mk" -o
        -name "*,fe1" -o
        -name "AutoGenMfS"); do awk '
        ''STATE==2                    { print "'$f' contains unstripped dynamic dependencies"; exit 1 }
        ''STATE==0 && /^ /            { print "'$f' line "NR" should start with a tab character" }
        ''                            { STATE=0 }
        ''/\\$/                       { STATE=1 }
        ''/^# Dynamic dependencies:$/ { STATE=2 }
        ' $f; done
  allow_failure: true

head_whitesp:
  stage: static_analysis
  tags: [ cross ]
  script:
    - >
        status=0;
        exceptions=();
        while read -d " " part; do
        if [ "$part" != "" ]; then
        exceptions=("${exceptions[@]}" -path "$part" -prune -o);
        fi;
        done <<< "$WHITESPACE_WHITELIST ";
        find . "${exceptions[@]}"
        -path "./.git" -prune -o
        -path "./Resources*/Messages" -prune -o
        -type f -print |
        xargs grep -In $'\(^  *\t\|[\t ]$\)'
        || status=1; test $status != 0
  allow_failure: true

merge_whitesp:
  stage: static_analysis
  tags: [ cross ]
  rules:
    - if: $CI_MERGE_REQUEST_ID
  script:
    - >
        git remote show | grep target > /dev/null && git remote remove target || true;
        git remote add -t $CI_MERGE_REQUEST_TARGET_BRANCH_NAME -f target $CI_MERGE_REQUEST_PROJECT_URL.git;
        awk '
        ''BEGIN {
        ''  result = 0;
        ''  cmd = "git diff -b -U0 target/'$CI_MERGE_REQUEST_TARGET_BRANCH_NAME' HEAD";
        ''  while ((cmd | getline) > 0) {
        ''    if ($1 == "+++")
        ''      file = gensub(/^b\//, "", "1", $2);
        ''    else if ($1 == "@@")
        ''      line = gensub(/+([0-9]+).*/, "\\1", "1", $3);
        ''    else if ($0 ~ /^+/)
        ''      change[file][line++];
        ''  }
        ''  close(cmd);
        ''  cmd = "git diff -U0 target/'$CI_MERGE_REQUEST_TARGET_BRANCH_NAME' HEAD";
        ''  while ((cmd | getline) > 0) {
        ''    if ($1 == "+++") {
        ''      file = gensub(/^b\//, "", "1", $2);
        ''      change[file]["dummy"]
        ''    } else if ($1 == "@@")
        ''      line = gensub(/+([0-9]+).*/, "\\1", "1", $3);
        ''    else if ($0 ~ /^+/) {
        ''      sub(/^+/, "");
        ''      if ($0 ~ /( +\t|[\t ]$)/) {
        ''        print file " line " line " adds whitespace error";
        ''        result = 1
        ''      } else if (!(line in change[file])) {
        ''        print file " line " line " only removes whitespace error";
        ''        result = 1
        ''      }
        ''      ++line
        ''    }
        ''  }
        ''  close(cmd)
        ''}
        ''END { exit result }'
  allow_failure: true

softload:
  stage: softload
  tags: [ cross ]
  script:
    - 'curl --location --output ~/cache/common/Disc.zip -z ~/cache/common/Disc.zip "https://gitlab.riscosopen.org/Products/Disc/-/jobs/artifacts/master/download?job=latest_package_tree"'
    - unzip -q ~/cache/common/Disc.zip && rm -rf RiscOS/Install
    - source RiscOS/Env/ROOL/Disc.sh
    - COMPONENT=Australia TARGET=Australia INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Australia TARGET=Australia INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Australia TARGET=Australia INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Australia TARGET=Australia INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Canada1 TARGET=Canada1 INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Canada1 TARGET=Canada1 INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Canada1 TARGET=Canada1 INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Canada1 TARGET=Canada1 INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=China TARGET=China INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=China TARGET=China INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=China TARGET=China INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=China TARGET=China INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Denmark TARGET=Denmark INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Denmark TARGET=Denmark INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Denmark TARGET=Denmark INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Denmark TARGET=Denmark INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Finland TARGET=Finland INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Finland TARGET=Finland INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Finland TARGET=Finland INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Finland TARGET=Finland INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=France TARGET=France INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=France TARGET=France INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=France TARGET=France INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=France TARGET=France INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Germany TARGET=Germany INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Germany TARGET=Germany INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Germany TARGET=Germany INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Germany TARGET=Germany INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Iceland TARGET=Iceland INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Iceland TARGET=Iceland INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Iceland TARGET=Iceland INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Iceland TARGET=Iceland INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Ireland TARGET=Ireland INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Ireland TARGET=Ireland INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Ireland TARGET=Ireland INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Ireland TARGET=Ireland INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Italy TARGET=Italy INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Italy TARGET=Italy INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Italy TARGET=Italy INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Italy TARGET=Italy INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Japan TARGET=Japan INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Japan TARGET=Japan INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Japan TARGET=Japan INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Japan TARGET=Japan INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Korea TARGET=Korea INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Korea TARGET=Korea INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Korea TARGET=Korea INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Korea TARGET=Korea INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Netherlands TARGET=Netherland INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Netherlands TARGET=Netherland INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Netherlands TARGET=Netherland INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Netherlands TARGET=Netherland INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Norway TARGET=Norway INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Norway TARGET=Norway INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Norway TARGET=Norway INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Norway TARGET=Norway INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Portugal TARGET=Portugal INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Portugal TARGET=Portugal INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Portugal TARGET=Portugal INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Portugal TARGET=Portugal INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=SAfrica TARGET=SAfrica INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=SAfrica TARGET=SAfrica INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=SAfrica TARGET=SAfrica INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=SAfrica TARGET=SAfrica INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Spain TARGET=Spain INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Spain TARGET=Spain INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Spain TARGET=Spain INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Spain TARGET=Spain INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Sweden TARGET=Sweden INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Sweden TARGET=Sweden INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Sweden TARGET=Sweden INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Sweden TARGET=Sweden INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Taiwan TARGET=Taiwan INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Taiwan TARGET=Taiwan INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Taiwan TARGET=Taiwan INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Taiwan TARGET=Taiwan INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Turkey TARGET=Turkey INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Turkey TARGET=Turkey INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Turkey TARGET=Turkey INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Turkey TARGET=Turkey INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=UK TARGET=UK INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=UK TARGET=UK INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=UK TARGET=UK INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=UK TARGET=UK INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=USA TARGET=USA INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=USA TARGET=USA INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=USA TARGET=USA INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=USA TARGET=USA INSTDIR=$INSTALLDIR make -k install
  artifacts:
    paths:
      - RiscOS/Install/*
  # For now, we expect some failed components
  allow_failure: true

softload_gnu:
  stage: softload
  tags: [ cross ]
  variables:
    TOOLCHAIN: GNU
  script:
    - 'curl --location --output ~/cache/common/Disc.zip -z ~/cache/common/Disc.zip "https://gitlab.riscosopen.org/Products/Disc/-/jobs/artifacts/master/download?job=latest_package_tree"'
    - unzip -q ~/cache/common/Disc.zip && rm -rf RiscOS/Install
    - source RiscOS/Env/ROOL/Disc.sh
    - COMPONENT=Australia TARGET=Australia INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Australia TARGET=Australia INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Australia TARGET=Australia INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Australia TARGET=Australia INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Canada1 TARGET=Canada1 INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Canada1 TARGET=Canada1 INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Canada1 TARGET=Canada1 INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Canada1 TARGET=Canada1 INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=China TARGET=China INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=China TARGET=China INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=China TARGET=China INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=China TARGET=China INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Denmark TARGET=Denmark INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Denmark TARGET=Denmark INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Denmark TARGET=Denmark INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Denmark TARGET=Denmark INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Finland TARGET=Finland INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Finland TARGET=Finland INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Finland TARGET=Finland INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Finland TARGET=Finland INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=France TARGET=France INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=France TARGET=France INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=France TARGET=France INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=France TARGET=France INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Germany TARGET=Germany INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Germany TARGET=Germany INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Germany TARGET=Germany INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Germany TARGET=Germany INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Iceland TARGET=Iceland INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Iceland TARGET=Iceland INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Iceland TARGET=Iceland INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Iceland TARGET=Iceland INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Ireland TARGET=Ireland INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Ireland TARGET=Ireland INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Ireland TARGET=Ireland INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Ireland TARGET=Ireland INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Italy TARGET=Italy INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Italy TARGET=Italy INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Italy TARGET=Italy INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Italy TARGET=Italy INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Japan TARGET=Japan INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Japan TARGET=Japan INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Japan TARGET=Japan INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Japan TARGET=Japan INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Korea TARGET=Korea INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Korea TARGET=Korea INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Korea TARGET=Korea INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Korea TARGET=Korea INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Netherlands TARGET=Netherland INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Netherlands TARGET=Netherland INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Netherlands TARGET=Netherland INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Netherlands TARGET=Netherland INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Norway TARGET=Norway INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Norway TARGET=Norway INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Norway TARGET=Norway INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Norway TARGET=Norway INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Portugal TARGET=Portugal INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Portugal TARGET=Portugal INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Portugal TARGET=Portugal INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Portugal TARGET=Portugal INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=SAfrica TARGET=SAfrica INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=SAfrica TARGET=SAfrica INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=SAfrica TARGET=SAfrica INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=SAfrica TARGET=SAfrica INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Spain TARGET=Spain INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Spain TARGET=Spain INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Spain TARGET=Spain INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Spain TARGET=Spain INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Sweden TARGET=Sweden INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Sweden TARGET=Sweden INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Sweden TARGET=Sweden INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Sweden TARGET=Sweden INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Taiwan TARGET=Taiwan INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Taiwan TARGET=Taiwan INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Taiwan TARGET=Taiwan INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Taiwan TARGET=Taiwan INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=Turkey TARGET=Turkey INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=Turkey TARGET=Turkey INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=Turkey TARGET=Turkey INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=Turkey TARGET=Turkey INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=UK TARGET=UK INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=UK TARGET=UK INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=UK TARGET=UK INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=UK TARGET=UK INSTDIR=$INSTALLDIR make -k install
    - COMPONENT=USA TARGET=USA INSTDIR=$INSTALLDIR make -k clean
    - COMPONENT=USA TARGET=USA INSTDIR=$INSTALLDIR make -k export_hdrs || true
    - COMPONENT=USA TARGET=USA INSTDIR=$INSTALLDIR make -k export_libs || true
    - COMPONENT=USA TARGET=USA INSTDIR=$INSTALLDIR make -k install
  artifacts:
    paths:
      - RiscOS/Install/*
  # For now, we expect some failed components
  allow_failure: true

rom_BCM2835:
  stage: rom
  tags: [ cross ]
  script:
    - 'curl --location --output ~/cache/common/BCM2835.zip -z ~/cache/common/BCM2835.zip "https://gitlab.riscosopen.org/Products/BCM2835/-/jobs/artifacts/master/download?job=latest_package_tree"'
    - unzip -q ~/cache/common/BCM2835.zip && mkdir -p RiscOS/Sources/Internat/Territory/TerritoryModule && rm -rf RiscOS/Sources/Internat/Territory/TerritoryModule && ln -s ../../../.. RiscOS/Sources/Internat/Territory/TerritoryModule
    - source RiscOS/Env/ROOL/BCM2835.sh
    # Run all rom build phases, stopping at the first one that fails (if any)
    - srcbuild export_hdrs
    - srcbuild export_libs
    - srcbuild resources
    - srcbuild rom
    - srcbuild install_rom
    - srcbuild join
  artifacts:
    paths:
      - RiscOS/Images/b*
  # For now, we expect some failed components
  allow_failure: true

rom_IOMD32:
  stage: rom
  tags: [ cross ]
  script:
    - 'curl --location --output ~/cache/common/IOMD32.zip -z ~/cache/common/IOMD32.zip "https://gitlab.riscosopen.org/Products/IOMDHAL/-/jobs/artifacts/master/download?job=latest_package_tree"'
    - unzip -q ~/cache/common/IOMD32.zip && mkdir -p RiscOS/Sources/Internat/Territory/TerritoryModule && rm -rf RiscOS/Sources/Internat/Territory/TerritoryModule && ln -s ../../../.. RiscOS/Sources/Internat/Territory/TerritoryModule
    - source RiscOS/Env/ROOL/IOMD32.sh
    # Run all rom build phases, stopping at the first one that fails (if any)
    - srcbuild export_hdrs
    - srcbuild export_libs
    - srcbuild resources
    - srcbuild rom
    - srcbuild install_rom
    - srcbuild join
  artifacts:
    paths:
      - RiscOS/Images/b*
  # For now, we expect some failed components
  allow_failure: true

rom_OMAP3:
  stage: rom
  tags: [ cross ]
  script:
    - 'curl --location --output ~/cache/common/OMAP3.zip -z ~/cache/common/OMAP3.zip "https://gitlab.riscosopen.org/Products/OMAP3/-/jobs/artifacts/master/download?job=latest_package_tree"'
    - unzip -q ~/cache/common/OMAP3.zip && mkdir -p RiscOS/Sources/Internat/Territory/TerritoryModule && rm -rf RiscOS/Sources/Internat/Territory/TerritoryModule && ln -s ../../../.. RiscOS/Sources/Internat/Territory/TerritoryModule
    - source RiscOS/Env/ROOL/OMAP3.sh
    # Run all rom build phases, stopping at the first one that fails (if any)
    - srcbuild export_hdrs
    - srcbuild export_libs
    - srcbuild resources
    - srcbuild rom
    - srcbuild install_rom
    - srcbuild join
  artifacts:
    paths:
      - RiscOS/Images/b*
  # For now, we expect some failed components
  allow_failure: true

rom_OMAP4:
  stage: rom
  tags: [ cross ]
  script:
    - 'curl --location --output ~/cache/common/OMAP4.zip -z ~/cache/common/OMAP4.zip "https://gitlab.riscosopen.org/Products/OMAP4/-/jobs/artifacts/master/download?job=latest_package_tree"'
    - unzip -q ~/cache/common/OMAP4.zip && mkdir -p RiscOS/Sources/Internat/Territory/TerritoryModule && rm -rf RiscOS/Sources/Internat/Territory/TerritoryModule && ln -s ../../../.. RiscOS/Sources/Internat/Territory/TerritoryModule
    - source RiscOS/Env/ROOL/OMAP4.sh
    # Run all rom build phases, stopping at the first one that fails (if any)
    - srcbuild export_hdrs
    - srcbuild export_libs
    - srcbuild resources
    - srcbuild rom
    - srcbuild install_rom
    - srcbuild join
  artifacts:
    paths:
      - RiscOS/Images/b*
  # For now, we expect some failed components
  allow_failure: true

rom_OMAP5:
  stage: rom
  tags: [ cross ]
  script:
    - 'curl --location --output ~/cache/common/OMAP5.zip -z ~/cache/common/OMAP5.zip "https://gitlab.riscosopen.org/Products/OMAP5/-/jobs/artifacts/master/download?job=latest_package_tree"'
    - unzip -q ~/cache/common/OMAP5.zip && mkdir -p RiscOS/Sources/Internat/Territory/TerritoryModule && rm -rf RiscOS/Sources/Internat/Territory/TerritoryModule && ln -s ../../../.. RiscOS/Sources/Internat/Territory/TerritoryModule
    - source RiscOS/Env/ROOL/OMAP5.sh
    # Run all rom build phases, stopping at the first one that fails (if any)
    - srcbuild export_hdrs
    - srcbuild export_libs
    - srcbuild resources
    - srcbuild rom
    - srcbuild install_rom
    - srcbuild join
  artifacts:
    paths:
      - RiscOS/Images/b*
  # For now, we expect some failed components
  allow_failure: true

rom_Titanium:
  stage: rom
  tags: [ cross ]
  script:
    - 'curl --location --output ~/cache/common/Titanium.zip -z ~/cache/common/Titanium.zip "https://gitlab.riscosopen.org/Products/Titanium/-/jobs/artifacts/master/download?job=latest_package_tree"'
    - unzip -q ~/cache/common/Titanium.zip && mkdir -p RiscOS/Sources/Internat/Territory/TerritoryModule && rm -rf RiscOS/Sources/Internat/Territory/TerritoryModule && ln -s ../../../.. RiscOS/Sources/Internat/Territory/TerritoryModule
    - source RiscOS/Env/ROOL/Titanium.sh
    # Run all rom build phases, stopping at the first one that fails (if any)
    - srcbuild export_hdrs
    - srcbuild export_libs
    - srcbuild resources
    - srcbuild rom
    - srcbuild install_rom
    - srcbuild join
  artifacts:
    paths:
      - RiscOS/Images/b*
  # For now, we expect some failed components
  allow_failure: true

rom_Tungsten:
  stage: rom
  tags: [ cross ]
  script:
    - 'curl --location --output ~/cache/common/Tungsten.zip -z ~/cache/common/Tungsten.zip "https://gitlab.riscosopen.org/Products/Tungsten/-/jobs/artifacts/master/download?job=latest_package_tree"'
    - unzip -q ~/cache/common/Tungsten.zip && mkdir -p RiscOS/Sources/Internat/Territory/TerritoryModule && rm -rf RiscOS/Sources/Internat/Territory/TerritoryModule && ln -s ../../../.. RiscOS/Sources/Internat/Territory/TerritoryModule
    - source RiscOS/Env/ROOL/Tungsten.sh
    # Run all rom build phases, stopping at the first one that fails (if any)
    - srcbuild export_hdrs
    - srcbuild export_libs
    - srcbuild resources
    - srcbuild rom
    - srcbuild install_rom
    - srcbuild join
  artifacts:
    paths:
      - RiscOS/Images/b*
  # For now, we expect some failed components
  allow_failure: true

rom_iMx6:
  stage: rom
  tags: [ cross ]
  script:
    - 'curl --location --output ~/cache/common/iMx6.zip -z ~/cache/common/iMx6.zip "https://gitlab.riscosopen.org/Products/iMx6/-/jobs/artifacts/master/download?job=latest_package_tree"'
    - unzip -q ~/cache/common/iMx6.zip && mkdir -p RiscOS/Sources/Internat/Territory/TerritoryModule && rm -rf RiscOS/Sources/Internat/Territory/TerritoryModule && ln -s ../../../.. RiscOS/Sources/Internat/Territory/TerritoryModule
    - source RiscOS/Env/ROOL/iMx6.sh
    # Run all rom build phases, stopping at the first one that fails (if any)
    - srcbuild export_hdrs
    - srcbuild export_libs
    - srcbuild resources
    - srcbuild rom
    - srcbuild install_rom
    - srcbuild join
  artifacts:
    paths:
      - RiscOS/Images/b*
  # For now, we expect some failed components
  allow_failure: true

cleanup:
  stage: cleanup
  tags: [ cross ]
  script:
  - git clean -xdf
